# freeculture-docs

Policies, documents, research around intellectual property. Benchmarking. Cases. Studies. You name them...

Políticas, leyes, documentos, investigaciones en torno a propiedad intelectual. Comparativos. Casos. Casos de estudio. Tú dices.


## Directrices para archivos ##

- Documentos en formato markdown o formato pandoc-markdown
- Metadatos en bloques YAML al principio del archivo
- Orden canónico para nombrar archivos:
  >  COD-AuthorLastname,_AuthorFirstname_AuthorLastname,_AuthorFirstname_-_Nombre_del_trabajo.md

