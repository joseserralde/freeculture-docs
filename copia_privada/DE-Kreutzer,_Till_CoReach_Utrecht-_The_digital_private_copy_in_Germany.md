---
title: The digital private copy in Germany
lang: en
subtitle: Review of the development and effects of former and prospective reforms of the private copying rule in Art. 53 Urheberrechtsgesetz (German Copyright Act)
author: by Till Kreutzer, PhD, IP Lawyer, Research Fellow at the Hans–Bredow– Institute, Member of the German Commission for UNESCO
address: | i.e. Büro.Hamburg
  Mittelweg 177
  20148 Hamburg
  Fon. [+49] [40] 229 48 56-0
  Fax. [+49] [40] 229 48 56-3
  <www.ie-online.de>
...

Source
: <http://www.coreach-ipr.org/documents/CoReach%20Utrecht-Private%20copying%20in%20Germany.pdf>  
Transcribed and converted by José María Serralde Ruiz from original presentation slides.

## AGENDA

1. History: The private copying rule in German Copyright Law

2. .Development: The reassessment of Art. 53 UrhG in the digital age

3. Analysis: The effects of the recent reforms on private digital copies

4. Outlook: The intended changes of Art. 53 UrhG in the “Third Basket”

5. Recommendations

## The beginning

First introduction of a modern private copying rule in the fundamental reform of the German Copyright Act in 1965

- Reason. Marketing of the first technical equipment for mechanical copying (magnetic tape recorders, first photocopy machines)

- Reasoning

  1. Private realm is uncontrollable

  2. Control of private realm (by collecting societies) would infringe rights to privacy

### Compensation

Levy for copies on storage media (images and sound) – not for reprographical copying

Early further development:

Copyright reform 1985 –
Extension of the right to remuneration and the levy system to reprographical copies 


## Development: The reassessment of Art. 53 UrhG in the digital age

The first “Act to reform copyright law in the Information Society” (“First Basket”) – coming into effect in September 2003

Intention
: Implementation of the InfoSoc– Directive 2001/29/EG

Reassessment of the private copying rule
: Art. 53 sec. 1 UrhG shall apply to private copies, notwithstanding their format (analogue, digital)

Changes to the private copying
:
rule 1:
The protection against the circumvention of TPM/DRMS overrules digital private copying. No enforcement of the (digital) private copy rule against TPM protection

Changes to the private copying

rule 2:
(Failed) try to reduce the
right to private copying
from legal sources in order
to ban downloads in
filesharing systems

Fundamental reform of the
private copying levy system:
1.The statutorily fixed rates
for certain copying devices
and storage media were
abolished in favour of
contractual settlements
between the affected
parties.

Fundamental reform of the
private copying levy system:
2. The adequate compensation
for private copies must be
calculated on the actual
amount of copies made with
the respective device/stored
on the respective media.

Effect:
Illegal copies (e.g. made by
circumventing TPMs, made
from obviously illegal
sources) cannot be
compensated!

The “Second Act to reform
copyright law in the
Information
Society” (“Second Basket”) –
coming into effect in
January 2008

Changes to the private copying
rule:
Expansion of the restriction
of private copying from
illegal sources in order to
(effectively) ban downloads
in filesharing systems

No changes to the
interrelation between the
private copying rule and TPM
protection

The Act to implement the
Enforcement Directive –
coming into effect in
September 2009

1st change concerning private
copying:
Amendment concerning the
enforcement of copyright
against consumers:
Introduction of a new right to
information against ISPs

On demand of rights holders
ISPs have to disclose the
identity of users who
committed copyright
infringements on the
Internet (using a certain
IP–Adress)

Effect:
(Hundreds of) Thousands of court
procedures where initiated by
the rights owners (mainly the
music industry) to identify
“Uploaders” in filesharing
systems and to accordingly send
them cease–and–desist–letters or
sue them

2nd change concerning private
copying:
(Failed) Attempt to limit the
reimbursement of lawyer’s costs
for cease–and–desist–letters in
case of “simple” and “marginal”
copyright infringements committed
by consumers (esp. online) to
100 Euro

AGENDA
1

History: The private copying rule in German Copyright
Law

2

Development: The reassessment of Art. 53 UrhG in the
digital age

3

Analysis: The effects of the recent reforms on private
digital copies

4

Outlook: The intended changes of Art. 53 UrhG in the
“Third Basket”

5

Recommendations

The question:
Did the recent restrictions
and amendments of the
private copying rule benefit
the authors and/or the
industry?

The answer:
No positive effect whatsoever
can be observed!

1. The limitation to copying
from legal sources

1st effect:
The rights holders neither
control nor enforce the ban
on downloading from illegal
sources. Actually an
enforcement is (technically)
not even possible.

Reason:
Apparently the downloaders
(different from the
“uploaders”) cannot be
identified in filesharing
communications by their IP–
Addresses and therefore not
pursued

2nd effect:
The rights owners get no
compensation through the
levy system for the numerous
(since the reform:
illegitimate) downloads
anymore

This affects esp. the creators
many of whom derive a
significant part of their
income from levies

2. The priority of the
protection of TPM over the
digital private copy

Has widely the same effects as
other restrictions of the
private copying rule

Since copying technically
protected works is not legal
anymore, copies made under
the circumvention of TPMs
are no longer compensated
through the levy system.

Since TPMs or the legal
protection against
circumvention do not
effectively prevent users
from copying, numerous
(illegal) copies that are
actually made are not
compensated anymore.

3. Collateral Damage:
Art.53 UrhG – an enigma

Read and understand (no
kidding …)

Remember:
This rule addresses the
consumers and other non–
professional users!

AGENDA
1

History: The private copying rule in German Copyright
Law

2

Development: The reassessment of Art. 53 UrhG in the
digital age

3

Analysis: The effects of the recent reforms on private
digital copies

4

Outlook: The intended changes of Art. 53 UrhG in the
“Third Basket”

5

Recommendations

The parliament ordered the
Ministry of Justice to
evaluate the need to further
reforms of certain aspects
in copyright law

Relating the private copying rule the parliament requested the M.o.J. to evaluate certain demands of the music and film industry
(again):

1. The need to reduce the right to private copying from an own original (i.e. to a backup copy)

2. The need to interdict the development and distribution of “intelligent recording software”

Expected effects (if enacted):

- No control and enforcement possible

- Further reduction of the compensation

- More or less significant drawbacks esp. for the creators 

- (Re–)Liberalisation or simplification of the private copying rule is not part of the request of the parliament nor on the agenda of the Ministry of Justice

### Recommendations

1. Reduce restrictions to feasible and enforceable aspects

2. Return to the axiom: Compensate what you cannot control

3. Simplify statutes that address consumers and other non–professional users 

4. Protect user’s interests more effectively: Make statutorily exceptions contract–proof (mandatory)

Thank you for your
attention!
Dr. Till Kreutzer, www.ie–online.de
