---
status: draft
summary: The private copying exception, explained below, was first introduced in the UK on 1 October 2014 by the *[Copyright and Rights in Performance (Personal Copies for Private Use) Regulations](http://www.legislation.gov.uk/uksi/2014/2361/made)*. However, following a judicial review filed against the UK government, on 17 July 2015 the High Court [quashed](http://www.bailii.org/ew/cases/EWHC/Admin/2015/2041.html) the regulations introducing the exception. As a result, the private copying exception is no longer part of UK copyright law, and the commentary below no longer represents the current state of the law on private copying. The government has said it will take time to consider whether and how a new exception for private copying should be introduced. Should a new legislative solution be adopted, we will update this page accordingly.
...

Private Copying
===============

**Author: Martin Kretschmer**\
**Illustration: Davide Bonazzi**

According to Copyright Law, creators have several exclusive rights they can exercise to restrict others from using their work. These include, amongst others, the reproduction right. This means that, in principle, *any act of copying a protected work*, including for example saving a copy of a song to an external hard drive, needs to be authorised by the rightsholder. However, UK Copyright Law contains a 'private copying' exception which takes away the need to obtain such permission.

The private copying exception allows consumers to copy works protected by copyright from one device to another without infringing copyright. For example, consumers are allowed to copy a song from a CD to a MP3 player, to make a back-up copy of a DVD lawfully acquired, and to store in the cloud copies of music or ebooks they legally downloaded.

Implementing the [Copyright Directive](http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2001:167:0010:0019:EN:PDF "Directive 2001/29/EC"), most EU Member States have introduced a private copying exception which allows end users to make copies of protected works 'for private use and for ends that are neither directly nor indirectly commercial', without the need to obtain permission from rightsholders.

However, EU law recognises that the private copying exception may lead to economic harm for copyright owners. In such cases, copyright owners receive a payment known as 'fair compensation'. Many countries use a system of copyright levies, a sort of tax or charge on media or devices that enable copying, for example blank CDs and DVDs, MP3 players, scanners, memory cards and computers. Money collected is then divided among rightsholders. As explained below, UK Copyright Law provides a very narrow 'personal copies' exception that is designed to cause minimal damage to rightsholders, and thus the UK has decided that no compensation payment is due.

 

### UK legislation

Under UK Copyright Law, end users can make 'personal copies for private use' of content they 'lawfully acquired on a permanent basis'. This means that it is lawful to make copies of materials you have purchased, received as a gift or downloaded from legal sources. However, it is not permitted to copy content borrowed from a friend, rented or unlawfully obtained. Also, the copies made under this copyright exception cannot be used for commercial purposes.

With the expression 'private use', the law explicitly allows format-shifting (e.g. converting audio files from CDs into MP3 format) and making back-up copies. You can also store your personal copies in the cloud without infringing copyright, as long as the storage service is accessible only by you.

Unlike in other EU Member States, under the UK private copying exception it is still illegal to share personal copies with family or friends. However, by limiting the exception to these narrow uses, the UK government has tried to avoid the creation of a complex system of fair compensation as implemented in many EU countries.

UK Copyright Law also allows time-shifting of broadcasts. This means you can record a TV or radio broadcast for viewing or listening at a more convenient time.

 

### Technological Protection Measures

It is important to note that even after the implementation of the private copying exception, consumers are not allowed to break copy protection technology (in legal jargon: Technological Protection Measures, or TPMs) in order to benefit from the exception. Rightsholders often apply TPMs to prevent any use of the work that they have not authorised and TPMs are themselves legally protected as a form of quasi-copyright. This means that if the work you want to copy for private, non-commercial use is protected by a TPM, you cannot circumvent the technical protection even though your use would be a form of private copying. In cases in which permitted acts are prevented by TPMs, a [procedure exists](https://www.gov.uk/government/publications/technological-protection-measures-tpms-complaints-process "Complaints procedure") which should allow you to use the content, but this is so difficult that so far no one has followed it.

Another important element of the legislation is that contractual terms seeking 'to prevent or restrict the making of a copy' permitted under the exception are 'unenforceable' in law. In other words, the private copying exception cannot be overridden by contract.

 

###  

### Legal language:

*ACI Adam BV and Others v Stichting de Thuiskopie, Stichting Onderhandelingen Thuiskopie vergoeding*, Case C-435/12, Court of Justice of the European Union (CJEU), 10 April 2014

In this judgement the Court of Justice of the European Union ruled that 'EU law, in particular Article 5(2)(b)' of the Copyright Directive, 'read in conjunction with paragraph 5 of that article, must be interpreted as precluding national legislation \[...\] which does not distinguish the situation in which the source from which a reproduction for private use is made is lawful from that in which that source is unlawful'.

*Padawan SL v Sociedad General de Autores y Editores de España (SGAE)*, Case C-467/08, European Court of Justice (ECJ), 21 October 2010

In this decision the European Court of Justice held that 'the concept of "fair compensation" \[...\] must be regarded as an autonomous concept of European Union law and interpreted uniformly throughout the European Union'. With reference to Recitals 35 and 38 of the Copyright Directive, the Court found that 'fair compensation must necessarily be calculated on the basis of the criterion of the harm caused to authors of protected works by the introduction of the private copying'.

### Legal references:

Directive 2001/29/EC on the harmonisation of certain aspects of copyright and related rights in the information society:\
<http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2001:167:0010:0019:EN:PDF>

The law on personal copying in the United Kingdom is set out in Section 28B and Schedule 2 (1B) of the Copyright, Designs and Patents Act 1988. You can read these provisions here: <http://www.legislation.gov.uk/uksi/2014/2361/made>

The complaints procedure where TPMs prevent or restrict personal copying can be found in [Section 296ZEA](http://www.legislation.gov.uk/uksi/2014/2361/made "Section 296ZEA CDPA") of the Copyright, Designs and Patents Act 1988. You can find out more about this complaints procedure here: <https://www.gov.uk/government/publications/technological-protection-measures-tpms-complaints-process>

The law on 'Recording for purposes of time-shifting' in the United Kingdom is set out in Section 70 of the Copyright, Designs and Patents Act 1988: <http://www.legislation.gov.uk/ukpga/1988/48/section/70>

Digital Opportunity. A Review of Intellectual Property and Growth. An Independent Report by Professor Ian Hargreaves (2011), available at: <http://webarchive.nationalarchives.gov.uk/20140603093549/http://www.ipo.gov.uk/ipreview.htm>

Private Copying and Fair Compensation: An empirical study of copyright levies in Europe, by Professor Martin Kretschmer (2011), available at: <http://www.ipo.gov.uk/ipresearch-faircomp-full-201110.pdf>
:::
:::
:::
:::

::: {.et_pb_row .et_pb_row_2}
::: {.et_pb_column .et_pb_column_4_4 .et_pb_column_2}
::: {.et_pb_text .et_pb_module .et_pb_bg_layout_light .et_pb_text_align_left .et_pb_text_2}
::: {.et_pb_text_inner}
Related
-------
:::
:::

::: {.et_pb_blog_grid_wrapper}
::: {.et_pb_blog_grid .clearfix .et_pb_module .et_pb_bg_layout_light .et_pb_blog_0 data-columns=""}
::: {.et_pb_image_container}
[![Copying & Creativity](https://www.copyrightuser.org/wp-content/uploads/2017/07/CCC_Frames_0-400x250.jpg){width="400" height="250"} []{.et_overlay .et_pb_inline_icon data-icon="9"}](https://www.copyrightuser.org/create/creative-process/copying-creativity/){.entry-featured-image-url}
:::

[Copying & Creativity](https://www.copyrightuser.org/create/creative-process/copying-creativity/) {#copying-creativity .entry-title}
-------------------------------------------------------------------------------------------------

::: {.post-content}
A short video exploring the complex relationship between copying and creativity through the eyes of a young art student.
:::

::: {.et_pb_image_container}
[![Terms & Conditions](https://www.copyrightuser.org/wp-content/uploads/2017/05/Terms-and-Conditions-400x250.jpg){width="400" height="250"} []{.et_overlay .et_pb_inline_icon data-icon="9"}](https://www.copyrightuser.org/understand/rights-permissions/terms-conditions/){.entry-featured-image-url}
:::

[Terms & Conditions](https://www.copyrightuser.org/understand/rights-permissions/terms-conditions/) {#terms-conditions .entry-title}
---------------------------------------------------------------------------------------------------

::: {.post-content}
Terms and conditions are a set of rules. These rules generally form a contract between you, the user, and the service provider, whose website you are visiting.
:::

::: {.et_pb_image_container}
[![Copyright Bite \#3](https://www.copyrightuser.org/wp-content/uploads/2017/06/3_Cover-400x250.jpg){width="400" height="250"} []{.et_overlay .et_pb_inline_icon data-icon="9"}](https://www.copyrightuser.org/create/public-domain/copyright-bite-3-permission-or-permitted/){.entry-featured-image-url}
:::

[Copyright Bite \#3](https://www.copyrightuser.org/create/public-domain/copyright-bite-3-permission-or-permitted/) {#copyright-bite-3 .entry-title}
------------------------------------------------------------------------------------------------------------------

::: {.post-content}
Copyright Bite \#3 considers how you can lawfully make use of, or borrow from, works that are still in copyright, but without having to ask for permission or make payment to the copyright owner.
:::
:::
:::
:::
:::
:::

::: {.et_pb_section .et_pb_section_3 .et_pb_with_background .et_section_regular}
::: {.et_pb_row .et_pb_row_3}
::: {.et_pb_column .et_pb_column_1_4 .et_pb_column_3}
::: {.et_pb_blurb .et_pb_module .et_pb_bg_layout_light .et_pb_text_align_left .et_pb_blurb_0 .et_pb_blurb_position_top}
::: {.et_pb_blurb_content}
::: {.et_pb_main_blurb_image}
[![](https://www.copyrightuser.org/wp-content/uploads/2017/02/create-logo_big.png){.et-waypoint .et_pb_animation_off}](http://www.create.ac.uk/)
:::

::: {.et_pb_blurb_container}
:::
:::
:::

::: {.et_pb_blurb .et_pb_module .et_pb_bg_layout_light .et_pb_text_align_left .et_pb_blurb_1 .et_pb_blurb_position_top}
::: {.et_pb_blurb_content}
::: {.et_pb_main_blurb_image}
[![](https://www.copyrightuser.org/wp-content/uploads/2017/02/glasgow-logo_big.png){.et-waypoint .et_pb_animation_off}](http://www.gla.ac.uk/)
:::

::: {.et_pb_blurb_container}
:::
:::
:::
:::

::: {.et_pb_column .et_pb_column_1_4 .et_pb_column_4}
::: {.et_pb_blurb .et_pb_module .et_pb_bg_layout_light .et_pb_text_align_left .et_pb_blurb_2 .et_pb_blurb_position_top}
::: {.et_pb_blurb_content}
::: {.et_pb_main_blurb_image}
[![](https://www.copyrightuser.org/wp-content/uploads/2017/02/CIPPM_new_logo.png){.et-waypoint .et_pb_animation_off}](http://www.cippm.org.uk/)
:::

::: {.et_pb_blurb_container}
:::
:::
:::

::: {.et_pb_blurb .et_pb_module .et_pb_bg_layout_light .et_pb_text_align_left .et_pb_blurb_3 .et_pb_blurb_position_top}
::: {.et_pb_blurb_content}
::: {.et_pb_main_blurb_image}
[![](https://www.copyrightuser.org/wp-content/uploads/2017/02/bu-logo-white_big.png){.et-waypoint .et_pb_animation_off}](https://www1.bournemouth.ac.uk/)
:::

::: {.et_pb_blurb_container}
:::
:::
:::
:::

::: {.et_pb_column .et_pb_column_1_4 .et_pb_column_5}
::: {.et_pb_blurb .et_pb_module .et_pb_bg_layout_light .et_pb_text_align_left .et_pb_blurb_4 .et_pb_blurb_position_top}
::: {.et_pb_blurb_content}
::: {.et_pb_main_blurb_image}
[![](https://www.copyrightuser.org/wp-content/uploads/2017/05/Queens.png){.et-waypoint .et_pb_animation_off}](http://www.qub.ac.uk/)
:::

::: {.et_pb_blurb_container}
:::
:::
:::

::: {.et_pb_blurb .et_pb_module .et_pb_bg_layout_light .et_pb_text_align_left .et_pb_blurb_5 .et_pb_blurb_position_top}
::: {.et_pb_blurb_content}
::: {.et_pb_main_blurb_image}
[![](https://www.copyrightuser.org/wp-content/uploads/2017/03/catapult_new_logo_trasparente.png){.et-waypoint .et_pb_animation_off}](https://www.digitalcatapultcentre.org.uk/?utm_source=Logo%20Backlink&utm_medium=Logo%20used%20by%20Partners&utm_campaign=Logo%20Backlink)
:::

::: {.et_pb_blurb_container}
:::
:::
:::
:::

::: {.et_pb_column .et_pb_column_1_4 .et_pb_column_6}
::: {.et_pb_blurb .et_pb_module .et_pb_bg_layout_light .et_pb_text_align_left .et_pb_blurb_6 .et_pb_blurb_position_top}
::: {.et_pb_blurb_content}
::: {.et_pb_main_blurb_image}
[![BFI film](https://www.copyrightuser.org/wp-content/uploads/2017/05/BFI.png){.et-waypoint .et_pb_animation_off}](http://www.bfi.org.uk/)
:::

::: {.et_pb_blurb_container}
:::
:::
:::

::: {.et_pb_blurb .et_pb_module .et_pb_bg_layout_light .et_pb_text_align_left .et_pb_blurb_7 .et_pb_blurb_position_top}
::: {.et_pb_blurb_content}
::: {.et_pb_main_blurb_image}
[![](https://www.copyrightuser.org/wp-content/uploads/2017/02/cemp_small.png){.et-waypoint .et_pb_animation_off}](https://www.cemp.ac.uk/)
:::

::: {.et_pb_blurb_container}
:::
:::
:::
:::
:::

::: {.et_pb_row .et_pb_row_4}
::: {.et_pb_column .et_pb_column_4_4 .et_pb_column_7}
::: {.et_pb_module .et_pb_space .et_pb_divider .et_pb_divider_0 .et_pb_divider_position_top .et-hide-mobile}
::: {.et_pb_divider_internal}
:::
:::
:::
:::

::: {.et_pb_row .et_pb_row_5}
::: {.et_pb_column .et_pb_column_1_4 .et_pb_column_8}
::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h4_footer .et_pb_text_3}
::: {.et_pb_text_inner}
#### [CREATE](https://www.copyrightuser.org/create/) 
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_4}
::: {.et_pb_text_inner}
##### [Creators Discuss](https://www.copyrightuser.org/create/creators-discuss/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_5}
::: {.et_pb_text_inner}
###### [Musician](https://www.copyrightuser.org/create/creators-discuss/musician/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_6}
::: {.et_pb_text_inner}
###### [Filmmaker](https://www.copyrightuser.org/create/creators-discuss/filmmaker/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_7}
::: {.et_pb_text_inner}
###### [Performer](https://www.copyrightuser.org/create/creators-discuss/performer/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_8}
::: {.et_pb_text_inner}
###### [Writer](https://www.copyrightuser.org/create/creators-discuss/writer/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_9}
::: {.et_pb_text_inner}
###### [Visual Artist](https://www.copyrightuser.org/create/creators-discuss/visual-artist/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_10}
::: {.et_pb_text_inner}
###### [Developer](https://www.copyrightuser.org/create/creators-discuss/developer/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_11}
::: {.et_pb_text_inner}
##### [Public Domain](https://www.copyrightuser.org/create/public-domain/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_12}
::: {.et_pb_text_inner}
###### [Public Domain: Duration](https://www.copyrightuser.org/create/public-domain/duration/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_13}
::: {.et_pb_text_inner}
###### [Copyright Bites](https://www.copyrightuser.org/create/public-domain/copyright-bites/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_14}
::: {.et_pb_text_inner}
###### [Copyright in SMEs](https://www.copyrightuser.org/create/public-domain/sme/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_15}
::: {.et_pb_text_inner}
##### [Lawful Reuse](https://www.copyrightuser.org/create/lawful-reuse/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_16}
::: {.et_pb_text_inner}
###### [Links & Resources](https://www.copyrightuser.org/create/lawful-reuse/links/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_17}
::: {.et_pb_text_inner}
##### [Creative Process](https://www.copyrightuser.org/create/creative-process/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_18}
::: {.et_pb_text_inner}
###### [Copyright & Creativity](https://www.copyrightuser.org/create/creative-process/copyright-and-creativity/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_19}
::: {.et_pb_text_inner}
###### [Copying & Creativity](https://www.copyrightuser.org/create/creative-process/copying-creativity/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_20}
::: {.et_pb_text_inner}
###### [Going for a Song](https://www.copyrightuser.org/create/creative-process/going-for-a-song/)
:::
:::
:::

::: {.et_pb_column .et_pb_column_1_4 .et_pb_column_9}
::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h4_footer .et_pb_text_21}
::: {.et_pb_text_inner}
#### [UNDERSTAND](https://www.copyrightuser.org/understand/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_22}
::: {.et_pb_text_inner}
##### [Rights & Permissions](https://www.copyrightuser.org/understand/rights-permissions/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_23}
::: {.et_pb_text_inner}
###### [Getting Permission](https://www.copyrightuser.org/understand/rights-permissions/getting-permission/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_24}
::: {.et_pb_text_inner}
###### [Legal Access](https://www.copyrightuser.org/understand/rights-permissions/legal-access/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_25}
::: {.et_pb_text_inner}
###### [Terms & Conditions](https://www.copyrightuser.org/understand/rights-permissions/terms-conditions/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_26}
::: {.et_pb_text_inner}
###### [Protecting](https://www.copyrightuser.org/understand/rights-permissions/protecting/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_27}
::: {.et_pb_text_inner}
###### [Licensing & Exploiting](https://www.copyrightuser.org/understand/rights-permissions/licensing-exploiting/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_28}
::: {.et_pb_text_inner}
###### [Using & Reusing](https://www.copyrightuser.org/understand/rights-permissions/using-reusing/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_29}
::: {.et_pb_text_inner}
###### [Enforcement](https://www.copyrightuser.org/understand/rights-permissions/enforcement/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_30}
::: {.et_pb_text_inner}
##### [Exceptions](https://www.copyrightuser.org/understand/exceptions/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_31}
::: {.et_pb_text_inner}
###### [Quotation](https://www.copyrightuser.org/understand/exceptions/quotation/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_32}
::: {.et_pb_text_inner}
###### [Parody & Pastiche](https://www.copyrightuser.org/understand/exceptions/parody-pastiche/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_33}
::: {.et_pb_text_inner}
###### [News Reporting](https://www.copyrightuser.org/understand/exceptions/news-reporting/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_34}
::: {.et_pb_text_inner}
###### [Education](https://www.copyrightuser.org/understand/exceptions/education/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_35}
::: {.et_pb_text_inner}
###### [Research & Private Study](https://www.copyrightuser.org/understand/exceptions/research-private-study/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_36}
::: {.et_pb_text_inner}
###### [Private Copying](https://www.copyrightuser.org/understand/exceptions/private-copying/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_37}
::: {.et_pb_text_inner}
###### [Orphan Works](https://www.copyrightuser.org/understand/exceptions/orphan-works/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_38}
::: {.et_pb_text_inner}
###### [Text & Data Mining](https://www.copyrightuser.org/understand/exceptions/text-data-mining/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_39}
::: {.et_pb_text_inner}
###### [Disabilities](https://www.copyrightuser.org/understand/exceptions/disability/)
:::
:::
:::

::: {.et_pb_column .et_pb_column_1_4 .et_pb_column_10}
::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h4_footer .et_pb_text_40}
::: {.et_pb_text_inner}
#### [EDUCATE](https://www.copyrightuser.org/educate/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_41}
::: {.et_pb_text_inner}
##### [The Game is On!](https://www.copyrightuser.org/educate/the-game-is-on/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_42}
::: {.et_pb_text_inner}
###### [Episode 1](https://www.copyrightuser.org/educate/the-game-is-on/episode-1/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_43}
::: {.et_pb_text_inner}
###### [Episode 2](https://www.copyrightuser.org/educate/the-game-is-on/episode-2/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_44}
::: {.et_pb_text_inner}
##### [A-Level Media Studies](https://www.copyrightuser.org/educate/copyright-level-media-studies/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_45}
::: {.et_pb_text_inner}
###### [Prompt one](https://www.copyrightuser.org/educate/a-level-media-studies/prompt-1/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_46}
::: {.et_pb_text_inner}
###### [Prompt two](https://www.copyrightuser.org/educate/a-level-media-studies/prompt-2/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_47}
::: {.et_pb_text_inner}
###### [Prompt three](https://www.copyrightuser.org/educate/a-level-media-studies/prompt-3/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_48}
::: {.et_pb_text_inner}
###### [Prompt four](https://www.copyrightuser.org/educate/a-level-media-studies/prompt-4/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_49}
::: {.et_pb_text_inner}
##### [Intermediaries](https://www.copyrightuser.org/educate/intermediaries/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_50}
::: {.et_pb_text_inner}
###### [Libraries](https://www.copyrightuser.org/educate/intermediaries/libraries/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_51}
::: {.et_pb_text_inner}
###### [Archives](https://www.copyrightuser.org/educate/intermediaries/archives/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_52}
::: {.et_pb_text_inner}
###### [Teachers & Students](https://www.copyrightuser.org/create/creators-discuss/teachers-students/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_53}
::: {.et_pb_text_inner}
##### [Enjoy ©](https://www.copyrightuser.org/educate/enjoy/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_54}
::: {.et_pb_text_inner}
###### [Myth-Reality Cards](https://www.copyrightuser.org/educate/enjoy/myth-reality-cards/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_55}
::: {.et_pb_text_inner}
##### [Copyright Bites](https://www.copyrightuser.org/create/public-domain/copyright-bites/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_56}
::: {.et_pb_text_inner}
###### [Copyright Bite \#1](https://www.copyrightuser.org/create/public-domain/copyright-bite-1-duration/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_57}
::: {.et_pb_text_inner}
###### [Copyright Bite \#2](https://www.copyrightuser.org/create/public-domain/copyright-bite-2-idea-expression/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .et_pb_text_58}
::: {.et_pb_text_inner}
###### [Copyright Bite \#3](https://www.copyrightuser.org/create/public-domain/copyright-bite-3-permission-or-permitted/)
:::
:::
:::

::: {.et_pb_column .et_pb_column_1_4 .et_pb_column_11}
::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h4_footer .et_pb_text_59}
::: {.et_pb_text_inner}
#### ABOUT
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_60}
::: {.et_pb_text_inner}
##### [About us](https://www.copyrightuser.org/about-us/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_61}
::: {.et_pb_text_inner}
##### [Methodology](https://www.copyrightuser.org/methodology/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_62}
::: {.et_pb_text_inner}
##### [FAQs](https://www.copyrightuser.org/faqs/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_63}
::: {.et_pb_text_inner}
##### [Video Gallery](https://www.copyrightuser.org/video-gallery/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_64}
::: {.et_pb_text_inner}
##### [Contact](https://www.copyrightuser.org/about-us/)
:::
:::

::: {.et_pb_text .et_pb_module .et_pb_bg_layout_dark .et_pb_text_align_left .h5_footer .et_pb_text_65}
::: {.et_pb_text_inner}
[[![](https://www.copyrightuser.org/wp-content/uploads/2017/08/CCby88x31.png){.alignnone .wp-image-4880 .size-full width="88" height="31"}](https://creativecommons.org/licenses/by/3.0/deed.en_US)]{style="font-size: 10pt;"}

[[Most of the original content on the Copyright User's website is distributed under a [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/deed.en_US) licence, meaning that you can share, remix, alter, and build upon Copyright User content for any purpose, as long as you [credit](https://www.copyrightuser.org/understand/rights-permissions/terms-conditions/) the author of the content. ]{style="color: #ffffff;"}[Where content on Copyright User is not distributed under a CC-BY 3.0 licence, this will be indicated clearly.]{style="color: #ffffff;"}]{style="font-size: 10pt;"} 
:::
:::
:::
:::
:::
:::

::: {.et_post_meta_wrapper}
:::
:::
:::
:::
:::

[]{.et_pb_scroll_top .et-pb-icon}

::: {#footer-bottom}
::: {.container .clearfix}
[ [![](https://copyrightuser.org/wp-content/uploads/2017/07/WK_Vector_Horiz_White.png)](https://worthknowing.org) ]{#sign}

-   [Twitter](https://twitter.com/copyrightuser){.icon}
:::
:::
:::
:::
